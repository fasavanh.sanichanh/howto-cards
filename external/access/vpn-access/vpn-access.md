---
card_order: 400
layout: page
permalink: /external/access/vpn-access/
shortcut: access:vpn-access
redirect_from:
  - /cards/access:vpn-access
  - /external/cards/access:vpn-access
  - /access/vpn-access
  - /external/external/access/vpn-access/
---
# VPN connection

VPN is used to access the servers that are hosted at the LCSB, but are not reachable "externally" from the internet, such as the [pre-publication check page](https://publications.lcsb.uni.lu) and the internal version of the [How-To cards](https://howto.lcsb.uni.lu). VPN uses the same user login information as the university active directory account (i.e., the same as used for Fiori, ServiceNow and Outlook e-mail), and is available automatically for all employees.


## Download and start the VPN

Once you have reset your VPN Password, please download VPN client from [vpn.uni.lu](https://vpn.uni.lu/).

**ENTER**

1. Username - firstname.lastname
2. Password - Password you have set for VPN

**Note**- If firstname.lastname does not work, give a try with firstname.lastname@uni.lu
<img src="img/vpn-download.png" height="300px"><br/>

Then depending on your distribution of OS, it will suggest you a link to download.
<img src="img/vpn-download-01.png" height="300px"><br/>


## Troubleshooting

Despite establishing the VPN connection, if you still cannot reach the internal websites and How-to cards, please make sure that you **do not use a custom DNS configuration**. The DNS configuration can be displayed under the **privacy and security options of your browser** or the **network settings of your operating system**.

<img src="img/dns-settings.png" height="300px"><br/>
