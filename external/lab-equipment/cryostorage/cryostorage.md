---
layout: page
permalink: /external/lab-equipment/cryostorage/
shortcut: lab:cryostorage
redirect_from:
  - /cards/lab-equipment:cryostorage
  - /external/cards/lab-equipment:cryostorage
  - /lab/cryostorage
  - /external/external/lab-equipment/cryostorage/
  - /cards/lab:cryostorage
  - /external/cards/lab:cryostorage
---

# Cryostorage: utilization

The BioStor III Cryo is a cryostorage system that offers sample
automation, cold chain management and security for the users. It is an
automated box-picking store which automates the selected rack extraction
and presents the requested cryobox to the user. It also minimizes the
other samples for the same rack and from the freezer from reaching a
temperature above 135°C. **When a rack is pulled out from the tank, the
users has to retrieve its sample within two minutes.** After this time,
the system will put the rack back into the freezer in order to maintain
the integrity of the samples.

If you want to use the cryostorage for the **first time**, please follow
the training available on [Docebo](https://unisupport.docebosaas.com/learn/course/47/play) and send a
[ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b) to the support team for the practical training.

If you notice any issue with the cryostorage, please contact the support
team **immediately**. If the issue is not critical, please send a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b).

## Safety rules

<div align="center">
<img src="img/image1.png" width="100" height="100">
<img src="img/image3.png" width="100" height="100">
</div>

<div align="center">
Please, wear your lab coat, safety googles -mandatory when handling cryopreserved samples and cryo gloves.
</div>

<div align="center">
<img src="img/image5.jpeg">
<img src="img/image6.png">
<img src="img/image7.jpeg" width="190" height="190">
</div>

## Description
<div align="center">
<img src="img/image8.jpeg" width="544" height="800">
</div>


| Number | Description |
| ----------- | ----------- |
| 1 | Rack Puller and Sleeve |
| 2 | Interactive Monitor |
| 3 | Lid Lifter and turntable Assembly |
| 4 | Chart Freezer |
| 5 | Access Door Module |


## Emergency procedure

If a rack get stuck in the tower, a **manual intervention** is mandatory to avoid any damage on the samples from this rack. **This action must be done in less than 2min to avoid the temperature to reach -135°C.**

1.  Switch off the automation power on the computer stand.
<div align="center">
<img src="img/image9.png">
</div>
2.  Open the tower door. The door is secured by magnetic latches.
<div align="center">
<img src="img/image10.png">
</div>
3. If a rack is stuck in the tower, push the brake release down so the
ball screw can be rotated.
<div align="center">
<img src="img/image11.png">

<img src="img/image12.png">
</div>
**The cover must be removed to have access to the brake. To do so, a
screw must be removed.**

4. Use a wrench on the flat part of the ball screw to lower the hand brake lever until you can reach the box. Hold the brake release while you pull the manual lever down.
<div align="center">
<img src="img/image13.png">
</div>
5.  Pull the manual brake lever down once you are able to reach the box to lower the rack back into the freezer. Hold the brake release while you pull the manual brake lever down.
<div align="center">
<img src="img/image14.png">
</div>
6.  Release the brake release
<div align="center">
<img src="img/image15.png">
</div>
7.  If a rack is stuck in the tower, release the rack from the gripper back into the dewar.
<div align="center">
<img src="img/image16.png">
</div>
**Now, the rack is safe. Please, do not forget to let the instrument
care team aware about this issue.\
Please, create a [ticket](https://service.uni.lu/sp?id=sc_cat_item&sys_id=be0873f8db7070505c72ef3c0b96199f&sysparm_category=af924f17dbac38105c72ef3c0b96194b) explaining the issue.**

## Operation

### Power On Sequence

1.  Turn on the main power switch and the automation power switch.
<div align="center">
<img src="img/image17.png">
</div>
2.  Turn on the computer power.
<div align="center">
<img src="img/image18.png">
</div>
3.  Sign in to the BioStore III. A login and a password will be provided to each user after completing the practical and theoretical trainings.
<div align="center">
<img src="img/image19.png">
</div>
4.  Click on **Go Online**
<div align="center">
<img src="img/image21.jpeg">
</div>

### Software Dashboard
<div align="center">
<img src="img/image21.jpeg">
</div>
| Number | Description |
| ----------- | ----------- |
| 1 | System States |
| 2 | System Controls |
| 3 | Orders |
| 4 | Capacity |
| 5 | Quick View of the Order Queue |
| 6 | LN2 Monitoring |
| 7 | Temperature Monitoring |
| 8 | Home Page Navigation Tabs |
| 9 | Drop-down UI |

### Freezer Layouts
<div align="center">
<img src="img/image22.jpeg">
</div>

## How to store a sample

1.  Make sure the system is initialized.
<div align="center">
<img src="img/image23.png">
</div>

2.  Click on **Orders**
<div align="center">
<img src="img/image24.png">
</div>

3.  Click on **Store**
<div align="center">
<img src="img/image25.png">
</div>
4.  Select the rack number in the freezer pane and select the shelf location in the rack pane.
<div align="center">
<img src="img/image26.png">
</div>
5.  Indicate your name in the **Order Name** and select the different parameters. Click on **Add Order**.
<div align="center">
<img src="img/image27.png">
</div>
**Note: the selected choice is the one in white.**

**Labware Type**
Only 2 models are available:
1.  In. (53 mm) 9x9 Cryo Box
2.  In. (50 mm) 10x10 Cryo Box

**Library**

Select a specific library to ensure your sample is stored in a partition assigned to the user group. Each user receives a dedicated partition related to his/her group.

**Destination Mode**

*User Specified*: the operator must select an empty location.

*System optimized*: the software will present the next empty box to the operator.

**Input Mode**

*Continuous*:**the rack will not be returned** to the freezer during data entry or confirmation.

*Integrity*: **return the rack** to the freezer during data entry or confirmation.

**Priority**

We recommend to always use the Normal priority mode.

## How to store a sample using a master file

A store order can be executed using a master file created by the user.
The master file must have a *.csv* extension in order to be imported. If
you want to use a master for the first time, please [contact](https://service.uni.lu/sp?id=sc_cat_item&sys_id=dc205fd7dbec38105c72ef3c0b96191d&sysparm_category=af924f17dbac38105c72ef3c0b96194b) the Support Team.

**Format for Boxes**
<div align="center">
<img src="img/image28.png">
</div>
**Format for Tubes**
<div align="center">
<img src="img/image29.png">
</div>
## How to retrieve a sample

1.  Make sure the system is initialized.
<div align="center">
<img src="img/image23.png">
</div>
2.  Click on **Orders**
<div align="center">
<img src="img/image24.png">
</div>
3.  Click on **Retrieve**
<div align="center">
<img src="img/image30.png">
</div>
### Retrieve a sample by its location

4.  Click on **By Location**
<div align="center">
<img src="img/image31.png">
</div>
5.  Select the **rack number** in the freezer pane and select the **box location** in the rack pane. This will add the labware to the order. Add as many boxes as necessary to the order. The requested items will be displayed on the right side of the screen.
<div align="center">
<img src="img/image32.png">
</div>
6.  Optional: indicate an Order Name. If not, it will be generated automatically. Select the different parameters and click on **Add Order**. A pop-up "order Added" will appear and the order is now queued for execution.
<div align="center">
<img src="img/image33.png">
</div>
***Mode***

*Return*: the box will be returned to the same location

*Remove*: the box will not be returned

**Sequence**

No difference between User Specified and System Optimized.

**Priority**

We recommend to always use the **Normal** priority mode

### Retrieve a sample by its ID

4. Click on **By ID**
<div align="center">
<img src="img/image34.png">
</div>
5. Enter the ID of the sample you are looking for.
<div align="center">
<img src="img/image35.png">
</div>
6. Click on **Add** to had the box to the order queue
<div align="center">
<img src="img/image36.png">
</div>
7. Confirm that all boxes selected for the order were added to the order queue.
<div align="center">
<img src="img/image37.png">
</div>
8. Optional: indicate an Order Name. If not, it will be generated automatically. Select the different parameters and click on **Add Order**. A pop-up "order Added" will appear and the order is now queued for execution.
<div align="center">
<img src="img/image33.png">
</div>
**Mode**

*Return*: the box will be returned to the same location

*Remove*: the box will not be returned

**Sequence**

No difference between User Specified and System Optimized.

**Priority**

We recommend to always use the **Normal** priority mode.

