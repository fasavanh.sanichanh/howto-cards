---
layout: page
permalink: /external/lab-hsa/pictograms/
shortcut: lab:pictograms
redirect_from:
  - /cards/lab-hsa:pictograms
  - /external/cards/lab-hsa:pictograms
  - /lab/pictograms
  - /external/external/lab-hsa/pictograms/
  - /cards/lab:pictograms
  - /external/cards/lab:pictograms
---


# Pictograms

Chemical risks in research laboratories are important to consider due to the numerous chemicals used and their various mixture. Knowing the signification of the hazard pictograms is therefore crucial. Reason why, before starting to work in the laboratories of the LCSB, it is mandatory to follow the Docebo trainging regarding the [Globally Harmonized System for the classification and labeling of chemicals](https://cdn5.dcbstatic.com/dcd/scormapi_v60/launcher_full.html?host=unisupport.docebosaas.com&id_user=13109&id_reference=137&scorm_version=1.2&id_resource=111&id_item=111&idscorm_organization=111&id_package=111&launch_type=fullscreen&id_course=18&player=hydra&autoplay_enabled=0&name=hazard-communication-standard-labels-and-pictograms&return_url=unisupport.docebosaas.com&as_json=1&auth_code=03e78b8ed77301828502c40972fbcadfdb5a249c&context=lms&rtl=false) and to pass the related Quizz.


Here is a summary of the pictograms that can be encountered in the laboratories. For more detailed information, you can see the [Table of concordance](https://www.yumpu.com/fr/document/read/19512777/produits-d-angereux-nouvel-etiquetage-tableau-de-concordance-1) (only in French for the moment).

To properly label your solutions, you can find some labels with the pictograms in the common stock of BT1 (-01 floor) and of BT2 (-02 floor).

## Health Hazard - CMR (Carcinogenic, Mutagenic, Reprotoxic) substances

- Carcinogens
- Mutagenicity
- Reproductive Toxicity
- Respiratory Sensitizer
- Target Organ Toxicity
- Aspiration Toxicity

<div align="center">
<img src="img/img8.png" width="25%"> 
</div>

## Flame

- Flammables
- Pyrophoric
- Self-heating
- Emits flammable Gas
- Self-reactive
- Organic Peroxides

<div align="center">
<img src="img/img3.png" width="25%">
</div>

## Exclamation Mark

- Irritant (eye, skin)
- Skin Sensitizer
- Acute Toxicity
- Narcotic Effects
- Respiratory tract irritant
- Hazardous to the Ozone layer

<div align="center">
<img src="img/img7.png" width="25%">
</div>

## Gas Cyclinder

- Gas under pressure

<div align="center">
<img src="img/img2.png" width="25%">
</div>

## Corrosive

- Skin corrosion/burns
- Eye damage
- Corrosive to metal

<div align="center">
<img src="img/img5.png" width="25%">
</div>

## Exploding Bomb

- Explosives
- Self-reactive
- Organic Peroxides

<div align="center">
<img src="img/img1.png" width="25%">
</div>

## Flame over circle

- Oxidizers

<div align="center">
<img src="img/img4.png" width="25%">
</div>

## Skull and crossbones

- Acute toxicity (Fatal or toxic)

<div align="center">
<img src="img/img6.png" width="25%">
</div>

## Environment

- Aquatic toxicity

<div align="center">
<img src="img/img9.png" width="25%">
</div>


